---
title: "Matriz de correlaciones con Corrplot"
author: "Marcel"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

En este tutorial veremos como graficar una matriz de correlaciones con el paquete *corrplot*.

En general partiremos de una matriz o data.frame de valores numéricos. Vamos a generar una para el tutorial:

```{r}
df<-data.frame("pats"=paste0("PAT", 1:20), "CytA"=rnorm(20,5), "CytB"=rnorm(20,5),"CytC"=c(rnorm(10,10),rnorm(10,5)),"CytD"=rnorm(20,5),"CytE"=c(rnorm(10,10),rnorm(10,5)),"CytF"=rnorm(20,5),"CytG"=c(rnorm(10,10),rnorm(10,5)))
head(df)
```

A partir del data.frame, generamos una matriz de correlaciones simétrica.

```{r}
mcor<-cor(df[,c(2:8)], df[,c(2:8)], use="pairwise.complete.obs", method = "spearman") # Por defecto usa el método de Pearson.
mcor
```

Además creamos una matriz con los p.values de estas correlaciones con la fución *cor.mtest* incluída en el paquete *corrplot*:

```{r}
library(corrplot)

mcor_pi<-cor.mtest(df[,c(2:8)], method = "spearman", na.rm=T)[[1]]
colnames(mcor_pi)<-colnames(mcor)
rownames(mcor_pi)<-rownames(mcor)
mcor_pi
```

Y ya estamos listos para usar la función *corrplot*. Por defecto genera este gráfico (tras incluirle la matriz de p.values):

```{r}
corrplot(mcor, sig.level=0.05, insig="label_sig", p.mat=mcor_pi)
```

Y se puede cambiar un poco la estética para que quede un poco más amigable:

```{r}
col2 <- colorRampPalette(rev(c("#67001F", "#B2182B", "#D6604D", "#F4A582",
                           "#FDDBC7", "#FFFFFF", "#D1E5F0", "#92C5DE",
                           "#4393C3", "#2166AC", "#053061")))
corrplot(mcor,type="lower", col=col2(200),tl.col= "black", pch.cex=1.3, pch.col="white", sig.level=0.05, insig="label_sig", p.mat=mcor_pi, 
         method="square", tl.cex=0.75, number.cex=0.6, mar=c(0,0,3,0), order = "hclust")
```

Además de los cambios estéticos (forma, color de la estadística, etc...) hay que resaltar que hemos **invertido el código de color** y que hemos indicado el parámetro `order = "hclust"`, lo que **ordena las correlaciones en función de un análisis jerárquicos de clusters**. Es muy útil para identificar qué variables se comportan de forma conjunta en las diferentes muestras.
