---
title: "Reshape"
author: "Marcel"
date: "3/3/2020"
output: html_document
---

El paquet [reshape2](https://cran.r-project.org/web/packages/reshape2/index.html) ens permet convertir una taula de forma *desordenada* a *ordenada*, i a l'inrevés. Per *ordenada* s'entén que cada fila correspon a una observació i totes les variables s'agrupen en una fila.

## Funció MELT

Creem una taula d'exemple:

```{r}
library(ggplot2)
library(reshape2)

table<-data.frame("patient"=paste0("#",1:10), "Group"=rep(c("Placebo", "DrugX"), 5),"VarA"=rnorm(10, mean=5), "VarB"=rnorm(10,mean=3), "VarC"=rnorm(10,mean=15))
table
```

Ara tenim una taula on la primera fila indica el pacient, la segona a quin grup de tractament correspon i les tres últimes indiquen valors de tres variables diferents mesurades als pacients.

Si nosaltres volem *ordenar* aquesta taula i respresentar-la amb ggplot necessitem que els valors de les tres variables estiguin en la mateixa columna.

Això ho aconseguim amb la funció **melt**:

```{r}
mtable<-melt(table)
head(mtable, n = 15)
```

Com veiem, la funció `melt` ja ens detecte quines són les variables numèriques i ens separa les que són caràcters (*patient* i *Group*).
Tot i així podem especificar-li quines columnes no volem que ens *ordeni* (per exemple si fossin numèriques). A més podem especificar-li un nom per les columnes on agregarà les variables i els valors d'aquestes variables.

```{r}
mtable<-melt(table, id=c("patient", "Group"), variable.name = "Conditions", value.name = "Measures")
head(mtable, n=15)
```

Ja tenim la taula adaptada per visualitzar les dades amb *ggplot2*.

```{r}
ggplot(mtable, aes(Conditions, Measures, fill=Group))+
  geom_bar(stat="summary", position=position_dodge())
```


## Funció DCAST

A vegades ens interessa compactar la taula (*desordenar*). Veurem un exemple semblant a la taula anterior. Enlloc de pacients amb diferents tractaments crearem mesures *pre* i *post* a un tractament de cada pacient. Després mirarem si hi ha diferències entre les dues mitjanes.

Creem la taula:
```{r}
table<-data.frame("patient"=rep(paste0("#",1:10), 2), "Time"=factor(rep(c("Pre", "Post"), each=10), levels=c("Pre", "Post")), "Measure"=c(rnorm(10, 1), rnorm(10,10)))
table
```

Amb la següent distribució:

```{r}
ggplot(table, aes(Time, Measure))+geom_jitter(width=0.2)
```


Ara Volem separar en dos columnes els valors de les mesures dels pacients *pre* i *post* tractament. Per això farem servir la funció **dcast**, la qual utilitza els següents arguments:

* **Dades**: la taula que farà servir.
* **fórmula**: la fórmula que li indicarà què utilitzar de files i què de columnes.
* **funció agregadora** (`fun.aggregate`): Utilitzada en cas que trobi més d'un valor per les coordinades de fila vs columna. Per defecte utilitza la llargada o el nombre d'aparicions. Però pot agregar les dades amb qualsevol funció (com `mean` o `max`).
* **Variable valor (`value.var`)**: Variables que contenen els valors. Per defecte ho intenta endevinar.

```{r}
un_table<-dcast(table, patient~Time)
un_table
```

Com veiem la funció a identificat correctament la columna *Measure* com la columna de valors.

Ara podem fer un test de Wilcoxon aparellat per veure si hi ha diferències estadístiques abans i després del tractament:

```{r}
wilcox.test(un_table$Pre, un_table$Post, paired=T)
```

I observem que sí que hi ha diferències significatives (p < 0.05).

També podem veure si hi ha correlació entre els nivells abans i després del tractament.
***Per veure correlacions sempre has de tenir els valors en dos columnes, ja que cada una de les variables és un eix diferent***

```{r}
ggplot(un_table, aes(Pre, Post))+
  geom_point()+
  geom_smooth(method="lm", se=F, color="black")

cor.test(un_table$Pre, un_table$Post)
```

Podem veure que no hi ha una correlació estadística.
