---
title: "Modificar Colores de un Gráfico"
author: "Marcel"
output: 
 html_document:
  toc: true
  toc_float: true
  toc_depth: 4
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

### Color en ggplot

El color en un gráfico de ggplot es una propiedad que puede afectar a la totalidad de una capa de forma uniforme, o estar mapeada a una escala y por lo tanto depender de una variable qualitativa o quantitativa.

En ggplot hay dos escalas que funcionan con "colores": la escala *color* (que se refiere al contorno de la forma) y la escala *fill* (que se refiere al interior de la forma).

Vamos a ver un ejemplo. Creamos un gráfico rápido:

```{r}
library(ggplot2)
tabla<-data.frame("Code"=paste0("Pac", 1:10), "Group"=rep(c("A","B"),each=5), "Valores"=rnorm(10, 20))
tabla
ggplot(tabla, aes(Code, Valores))+
  geom_bar(stat="identity")
```

Por defecto aparece con el relleno gris y el contorno transparente. Ahora vamos a rellenarlas de verde y el contorno negro:

```{r}
ggplot(tabla, aes(Code, Valores))+
  geom_bar(stat="identity", color="black", fill="light green")
```

Si os fijais, estos colores no dependen de ninguna escala. Vamos ahora a ligar el relleno a la variable *Group*:
```{r}
ggplot(tabla, aes(Code, Valores, fill=Group))+ # Fijaos que hemos añadido la escala "fill"
  geom_bar(stat="identity", color="black")
```

Ahora nos ha definido dos colores de relleno en función de las categorías de la variable *Group*. Si queremos cambiar la paleta que nos ofrece por defecto tenemos varias opciones. Primero crearemos otra tabla con más opciones en la variable *group*:

```{r}
tabla<-data.frame("Code"=paste0("Pac", 1:20), "Group"=rep(c("A","B","C","D"),each=5), "Valores"=rnorm(20, 20))
tabla
ggplot(tabla, aes(Code, Valores, fill=Group))+ # Fijaos que hemos añadido la escala "fill"
  geom_bar(stat="identity", color="black")+
    theme(axis.text.x = element_text(angle=45, hjust=1)) # Esto lo uso para poner en diagonal el eje de las X y evitar que se solapen
```

### Escalas discretas (categorías)
#### 1. Forma manual

Se pueden definir de forma manual mediante la capa `scale_fill_manual()`:

```{r}
ggplot(tabla, aes(Code, Valores, fill=Group))+ # Fijaos que hemos añadido la escala "fill"
  geom_bar(stat="identity", color="black")+
  scale_fill_manual(values=c("white","light coral","light green", "light blue"))+
  theme(axis.text.x = element_text(angle=45, hjust=1))
```

Los colores en R se pueden definir con dos códigos:

+ Nombre: un ejemplo es el que hemos usado antes, como "white" o "light coral".
+ Código HEX: código de 6 dígitos precedido por el caracter "#". Los equivalentes a white y light coral serían #FFFFFF y #F08080.

Así, vemos que los tres códigos son equivalentes:
```{r}
tabla_col<-data.frame("Name"=c("A","B"), "Valor"=c(5,10), "Col"=c("White", "Light Coral"))
gridExtra::grid.arrange(
  
  ggplot(tabla_col, aes(Name, Valor, fill=Col))+ 
  geom_bar(stat="identity", color="black")+
  scale_fill_manual(values=c("white","light coral"))+
  theme(axis.text.x = element_text(angle=45, hjust=1)),
  
  ggplot(tabla_col, aes(Name, Valor, fill=Col))+ 
  geom_bar(stat="identity", color="black")+
  scale_fill_manual(values=c("#FFFFFF","#F08080"))+
  theme(axis.text.x = element_text(angle=45, hjust=1)),
  
  nrow=1
  
)
```

Algo útil cuando defines manualmente el color es que puedes definir los valores con un vector *nombrado* (named vector) y especificar qué colores van con qué valores de la escala.

```{r}
ggplot(tabla, aes(Code, Valores, fill=Group))+ # Fijaos que hemos añadido la escala "fill"
  geom_bar(stat="identity", color="black")+
  scale_fill_manual(values=c("A"="white","C"="light coral","D"="light green","B"="light blue"))+
  theme(axis.text.x = element_text(angle=45, hjust=1))
```


#### 2. Mediante las paletas del paquete brewer.

Esto nos da la flexibilidad de elegir cualquier paleta de colores que queramos. Sin embargo, hay ya algunas funciones definidas que nos permitirán usar paletas del paquete *RColorBrewer*. Puedes consultarlo con este comando:

```{r, fig.height=10}
RColorBrewer::display.brewer.all()
```

Hay tres tipos de paletas: las sequenciales (primer grupo, adecuadas para datos ordenados que progresan de poco a mucho), las divergentes (tercer grupo, adecuadas para dar enfasis a puntos medios críticos y los extremos) y las qualitativas (segundo grupo, adecuadas para datos categóricos que no implican diferencias de magnitud entre clases).

Podemos acceder a esas paletas mediante la capa `scale_fill_brewer()`. Se puede definir el tipo de paleta ("seq","dev" o "qual", por defecto lo intenta adivinar por el tipo de dato) i un nombre o índice de paleta. Vamos a elegir la qualitativa "Pastel1":

```{r}
ggplot(tabla, aes(Code, Valores, fill=Group))+ # Fijaos que hemos añadido la escala "fill"
  geom_bar(stat="identity", color="black")+
  scale_fill_brewer(type="qual", palette = "Pastel1")+ # Al indicar el nombre de la paleta, no es necesario especificar el tipo.
  theme(axis.text.x = element_text(angle=45, hjust=1))
```

#### 2. Mediante las paletas del paquete viridis.

Otro paquete con cuatro escalas de colores secuenciales (aunque pueden usarse con datos qualitativos) es viridis:

![](https://www.datanovia.com/en/wp-content/uploads/dn-tutorials/ggplot2/figures/029-r-color-palettes-viridis-color-scales-1.png)

Las cuatro. Para usar una paleta discreta usaremos la capa `scale_fill_viridis_d()`:

```{r}
ggplot(tabla, aes(Code, Valores, fill=Group))+ # Fijaos que hemos añadido la escala "fill"
  geom_bar(stat="identity", color="black")+
  scale_fill_viridis_d()+
  theme(axis.text.x = element_text(angle=45, hjust=1))
```

Podemos cambiar la paleta con el parametro *option* ("magma" (o "A"), "inferno" (o "B"), "plasma" (o "C"), "viridis" (o "D", por defecto) y "cividis" (o "E"))

```{r}
ggplot(tabla, aes(Code, Valores, fill=Group))+ # Fijaos que hemos añadido la escala "fill"
  geom_bar(stat="identity", color="black")+
  scale_fill_viridis_d(option = "B")+
  theme(axis.text.x = element_text(angle=45, hjust=1))
```

#### 3. Mediante el paquete ggsci, con colores de revistas científicas.

El paquet ggsci tiene paletas de colores inspiradas en diferentes revistas científicas. Puedes consultarlas [aquí](https://cran.r-project.org/web/packages/ggsci/vignettes/ggsci.html). Por ejemplo, podemos usar la paleta inspirada en el grupo de publicación de nature (NPG):

```{r}
library(ggsci)
ggplot(tabla, aes(Code, Valores, fill=Group))+ # Fijaos que hemos añadido la escala "fill"
  geom_bar(stat="identity", color="black")+
  scale_fill_npg()+
  theme(axis.text.x = element_text(angle=45, hjust=1))
```

#### 4. En escala de grises

También se pueden generar en escala de grises:

```{r}
library(ggsci)
ggplot(tabla, aes(Code, Valores, fill=Group))+ # Fijaos que hemos añadido la escala "fill"
  geom_bar(stat="identity", color="black")+
  scale_fill_grey()+
  theme(axis.text.x = element_text(angle=45, hjust=1))
```



### Variables continuas

Para las escalas contínuas, puedes crear gradientes. Empezaremos generando una nueva tabla:
```{r}
tabla<-data.frame("genes"=LETTERS[1:10])
for (i in 1:5){
  tabla[paste0("Pop",i)]<-rnorm(10,3)
}
for (i in 6:10){
  tabla[paste0("Pop",i)]<-rnorm(10,10)
}
tabla
```

Esto es el formato ancho de una tabla, pero para usar con ggplot queremos el formato largo (una columna por dimensión).
```{r}
tabla<-reshape2::melt(tabla, variable.name="Pop")
head(tabla)
```


Y creamos un heatmap con el paquete ggplot con los colores por defecto:
```{r}
ggplot(tabla, aes(Pop, genes))+geom_tile(aes(fill=value))
```

#### 1. Gradientes
Por defecto coge un azul claro y otro oscuro para generar el gradiente. Si queremos especificar un color de valro bajo y uno de valor alto:
```{r}
ggplot(tabla, aes(Pop, genes))+
  geom_tile(aes(fill=value))+
  scale_fill_gradient(low="light blue", high = "light coral")
```

También se puede especificar un valor central, muy útil para aumentar el contraste en los valores extremos:
```{r}
ggplot(tabla, aes(Pop, genes))+
  geom_tile(aes(fill=value))+
  scale_fill_gradient2(low="light blue", high = "light coral", mid="khaki", midpoint = mean(tabla$value)) # Por defecto el valor medio es cero. Como nuestros valores no estan escalados al centro, hay que especificar un punto medio que sea la media de los valores.
```

Puedes añadir más colores al gradiente con la capa `scale_fill_gradientn(colours=c())`.

#### 2. Paquete Viridis

También puedes usar el paquete viridis, este caso para una escala continua con `scale_fill_viridis_c()`:
```{r}
ggplot(tabla, aes(Pop, genes))+
  geom_tile(aes(fill=value))+
  scale_fill_viridis_c()
```

#### 3. Paquete ggsci

El paquete ggsci también tiene un par de paletas continuas:

La de GSEA:
```{r}
ggplot(tabla, aes(Pop, genes))+
  geom_tile(aes(fill=value))+
  scale_fill_gsea()
```

Y una llamada "material design" que te crea un gradiente basado en un color:
```{r}
gridExtra::grid.arrange(
ggplot(tabla, aes(Pop, genes))+
  geom_tile(aes(fill=value))+
  scale_fill_material("red")+
  theme(axis.text.x = element_text(angle=45, hjust=1), aspect.ratio = 1),
ggplot(tabla, aes(Pop, genes))+
  geom_tile(aes(fill=value))+
  scale_fill_material("green")+
  theme(axis.text.x = element_text(angle=45, hjust=1), aspect.ratio = 1), nrow=1
)
```

Todas estas escalas se pueden usar también para el contorno usando la raíz `scale_color_*`.

### El relleno en las formas de puntos

Un caso particular son las formas basadas en puntos (geom_point o geom_jitter). La forma (shape) que usan por defecto no tiene relleno y usa el color del contorno. Un ejemplo:

```{r}
gridExtra::grid.arrange(

ggplot(tabla, aes(genes, value, color=Pop))+
  geom_point(fill="black", size=3),
ggplot(tabla, aes(genes, value, fill=Pop))+
  geom_point(color="black", size=3),

nrow=1)
```

Como veis, en el primer gráfico hemos definido color como escala basada en la variable "Pop" y el relleno como negro. Sin embargo los puntos han adoptado el color correspondiente a la variable "Pop". En cambio, en el segundo gráfico al definir el color como negro, no se ha hecho caso de la escala "fill" que habíamos definido. En definitiva, sin cambiar la forma lo que cuenta es la escala "color" y no la "fill.

Si queremos disociar el relleno del contorno necesitamos usar la forma número 21. Aquí puedes ver qué formas de punto hay disponibles y su índice:

![](http://www.sthda.com/sthda/RDoc/images/points-symbols.png)

Vamos a repetir el ejemplo anterior pero cambiando la forma:
```{r}
gridExtra::grid.arrange(

ggplot(tabla, aes(genes, value, color=Pop))+
  geom_point(fill="black", shape=21, size=3),
ggplot(tabla, aes(genes, value, fill=Pop))+
  geom_point(color="black", shape=21, size=3),

nrow=1)
```

### El color en las líneas

Las lineas usan la escala de *color*, nunca *fill*. Por ejemplo:

```{r}
ggplot(tabla, aes(genes, value, color=Pop))+
  geom_line(aes(group=Pop))
```

### Recursos para elegir paletas

- [Rapidtables](https://www.rapidtables.com/web/color/RGB_Color.html): útil para buscar códigos de colores.
- [ColorBrewer2](https://colorbrewer2.org/): útil para generar paletas.