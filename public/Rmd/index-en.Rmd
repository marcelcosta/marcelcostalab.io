---
title: "Index"
author: "Marcel"
output: html_document
---
[[Catala](index.html)][[English](index-en.html)]

1. [Variables](capitols/variables-en.html): description of available variables (lists and tables) and how to filter them.
2. [Data input](capitols/entrada-en.html): description of the different options to introduce your data into R from text files and spreadsheets.
3. [Reshape](capitols/reshape-en.html): usage of the **Reshape2** package to transform tables.
4. [ggplot](capitols/ggplot-en.html): usage of the **ggplot2** package to generate plots from tables.

ggplot customization:

1. [Sort x-axis with alfanumeric characters](capitols/pers-eixX-en.html).
2. [Modifying the color palette from a ggplot](capitols/mod-colors-en.html).
3. [Correlation matrices with Corrplot](capitols/corrplot-en.html).
4. [Generate heatmaps with ggplot package](capitols/heatmap-ggplots-en.html).